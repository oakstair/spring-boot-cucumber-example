package demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class JsonUtilsJacksonImpl implements JsonUtils {

    private static final Logger log = LoggerFactory.getLogger(JsonUtilsJacksonImpl.class);

    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public String toJson(Object object) {
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            log.error("Failed to create json", e);
            throw new IllegalArgumentException("Failed to produce JSON: " + e.getMessage());
        }
    }

    @Override
    public <T> T fromJson(String json, Class<T> clazz) {
        try {
            return mapper.readValue(json, clazz);
        } catch (IOException e) {
            log.error("Failed to parse json \"" + json + "\": " + e.getMessage(), e);
            throw new IllegalArgumentException("Failed to parse JSON \"" + json + "\" Error: " + e.getMessage());
        }
    }

}
