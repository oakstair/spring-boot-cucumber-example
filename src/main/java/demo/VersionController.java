package demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VersionController {

    @RequestMapping(value = "/version", method = RequestMethod.GET, produces = "application/json")
    public Version getVersion() {
        return new Version("0.0.1");
    }
}
