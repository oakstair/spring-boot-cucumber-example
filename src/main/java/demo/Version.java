package demo;

public class Version {
    private String major = "0";
    private String minor = "0";
    private String patch = "0";

    private Version() {
    }

    public Version(String version) {
        String v = version.trim();
        String[] sss = v.split("\\.");

        if (sss.length > 0)
            major = sss[0];
        if (sss.length > 1)
            minor = sss[1];
        if (sss.length > 2)
            patch = sss[2];
    }

    public Version(String major, String minor, String patch) {
        this.major = major;
        this.minor = minor;
        this.patch = patch;
    }

    public String getMajor() {
        return major;
    }
    public String getMinor() {
        return minor;
    }
    public String getPatch() {
        return patch;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        b.append(major);
        if (minor != null) {
            b.append('.');
            b.append(minor);
        }
        if (patch != null) {
            b.append('.');
            b.append(patch);
        }
        return b.toString();
    }

    @Override
    public boolean equals(Object o) {
        if ((o == null) || !(o instanceof Version))
            return false;

        Version v = (Version) o;
        return toString().equals(v.toString());
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
