package demo;

import org.springframework.stereotype.Service;

@Service
public interface JsonUtils {

    <T> T fromJson(String json, Class<T> clazz);

    String toJson(Object object);

}
