package demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DemoApplication.class})
public class JsonUtilsTest {

    @Autowired
    private JsonUtils jsn;


    @Test
    public void fromJson() throws Exception {
        Integer i = jsn.fromJson("12", Integer.class);
        assertEquals(12, (int) i);
    }

    @Test
    public void toJson() throws Exception {
        assertEquals("12", jsn.toJson(new Integer(12)));
    }

}
