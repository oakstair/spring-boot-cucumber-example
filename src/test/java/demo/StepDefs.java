package demo;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration
public class StepDefs {

    private HttpResponse lastResponse = null;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private JsonUtils jsn;

    @When("^the client calls /version$")
    public void the_client_calls_version() throws Throwable {
        lastResponse = urlFetch("/version");
    }

    @Then("^the client receives status code of (\\d+)$")
    public void the_client_receives_status_code_of(int statusCode) throws Throwable {
        assertThat("incorrect status code", lastResponse.status, is(statusCode));
    }

    @When("^the client calls /poop$")
    public void the_client_calls_poop() throws Throwable {
        lastResponse = urlFetch("/poop");
    }

    @And("^the client receives server version(.+)$")
    public void the_client_receives_server_version_body(String versionString) throws Throwable {
        Version expectedVersion = new Version(versionString);
        Version receivedVersion = jsn.fromJson(lastResponse.body, Version.class);
        assertThat(receivedVersion, is(expectedVersion));
    }

    @And("^the client receives poop (.+)$")
    public void the_client_receives_poop(String poop) throws Throwable {
        assertThat(lastResponse.body, is(poop));
    }

    /**
     * Call an url and return status + body.
     *
     * @param url
     * @return Status + body wrapped in a HttpResponse.
     * @throws IOException
     */
    private HttpResponse urlFetch(String url) throws IOException {
        //7 RestTemplate rest = new RestTemplate();

        ResponseEntity<String> resp = restTemplate.getForEntity(url, String.class);

        return new HttpResponse(resp.getStatusCodeValue(), resp.getBody());
    }

    // -------------------------------------------------------------------------
    // -- Private - Private - Private - Private - Private - Private - Private --
    // -------------------------------------------------------------------------

    private String fmt(String msg, Object... args) {
        return String.format(msg, args);
    }

    private static class HttpResponse {
        String body;
        int status;

        public HttpResponse(int status, String body) {
            this.status = status;
            this.body = body;
        }
    }

}
